//Nerf-C18.ino

#define FLYWHEEL_START_TIME 500	//发射电机起步加速时间

#define PIN_SWITCH_FLYWHEEL 2 		//飞轮开关
#define PIN_SWITCH_TRIGGER  3		//扳机开关
#define PIN_SWITCH_PUSHORD_RESET 4	//推杆复位开关
#define PIN_MOTOR_FLYWHEEL 5		//飞轮电源
#define PIN_MOTOR_PUSHORD 6		//推杆电源
#define PIN_MOTOR_FIRE_MODE 7			//射击模式切换

int stateSwitchFlywheel = 0;	//飞轮开关状态
int stateSwitchTrigger = 0;		//扳机开关状态
int stateSwitchSuperTrigger = 0;	//飞轮开关闭合状态下再闭合扳机就会1, 松开飞轮变0
int stateSwitchPushordReset = 0;	//推杆复位开关状态
int stateSwitchFireMode = 0;		//设计模式开关状态

unsigned long timeFlywheelSwitchOn = 0;
long fireCount = 0;	//射击数
long minFireCount = 0;	//
enum fireMode {
	single = 1, three = 3, continuous = 100
} fireMode;

void setup() {
  Serial.begin(9600);        //设置波特率为9600 bps
  pinMode(PIN_SWITCH_FLYWHEEL, INPUT);
  pinMode(PIN_SWITCH_TRIGGER, INPUT);
  pinMode(PIN_SWITCH_PUSHORD_RESET, INPUT);
  pinMode(PIN_MOTOR_FIRE_MODE, INPUT);
  pinMode(PIN_MOTOR_FLYWHEEL, OUTPUT);
  pinMode(PIN_MOTOR_PUSHORD, OUTPUT);

  digitalWrite(PIN_MOTOR_FLYWHEEL, 0);
  digitalWrite(PIN_MOTOR_PUSHORD, 0);

  fireMode = single;
}

void loop() {
  getSwitchState();

  Serial.println(fireCount);
  // delay(100);

  fireControl();

  // Serial.print("SuperTrigger:");
  // Serial.println(stateSwitchSuperTrigger);
  // delay(500);
}

void fireControl() {
	if (stateSwitchFlywheel == 0 && stateSwitchTrigger == 0 && stateSwitchPushordReset == 0) {
    // Serial.println("1");
    motorReset();
  } else if (stateSwitchSuperTrigger == 1) {
    // Serial.println("2");
    motorFire();
  } else if (stateSwitchPushordReset == 1) {
    // Serial.println("3");
    motorFire();
  } else if (stateSwitchFlywheel == 1) {
    // Serial.println("4");
    motorFlywheel();
  } else {
    // Serial.println("else");
    motorReset();
  }
}

//重置电机
void motorReset() {
	digitalWrite(PIN_MOTOR_FLYWHEEL, 0);
    digitalWrite(PIN_MOTOR_PUSHORD, 0);
}

//启动飞轮电机
void motorFlywheel() {
	digitalWrite(PIN_MOTOR_FLYWHEEL, 1);
    digitalWrite(PIN_MOTOR_PUSHORD, 0);
}

//发射(飞轮点击和推杆电机)
void motorFire() {
	digitalWrite(PIN_MOTOR_FLYWHEEL, 1);
    digitalWrite(PIN_MOTOR_PUSHORD, 1);
}

//获取开关状态
void getSwitchState() {
  int oldSwitchFlywheel = stateSwitchFlywheel;
  int oldStateSwitchPushordReset = stateSwitchPushordReset;
  int oldStateSwitchTrigger = stateSwitchTrigger;
  int oldStateSwitchFireMode = stateSwitchFireMode;

  stateSwitchFlywheel = digitalRead(PIN_SWITCH_FLYWHEEL);
  stateSwitchTrigger = digitalRead(PIN_SWITCH_TRIGGER);
  stateSwitchPushordReset = digitalRead(PIN_SWITCH_PUSHORD_RESET);
  stateSwitchFireMode = digitalRead(PIN_MOTOR_FIRE_MODE);

  if(!oldStateSwitchPushordReset && stateSwitchPushordReset){
      fireCount++;
      minFireCount++;
  }

  if(!oldStateSwitchTrigger && stateSwitchTrigger){
      minFireCount = 0;
  }

  //设计模式切换
  if(!oldStateSwitchFireMode && stateSwitchFireMode) {
  	//single, three, continuous
  	if(fireMode == single) {
  		fireMode = three;
  	} else if(fireMode == three){
  	    fireMode = continuous;
  	} else if(fireMode == continuous){
  	    fireMode = single;
  	}
  }

  // 电机转500毫秒以后才算电机开关打开
  int tempFlywheelSwitch;
  if (!oldSwitchFlywheel && stateSwitchFlywheel) {
    timeFlywheelSwitchOn = millis();
  }
  if ( (timeFlywheelSwitchOn + FLYWHEEL_START_TIME) < millis() && stateSwitchFlywheel) {
    tempFlywheelSwitch = 1;
  } else {
    tempFlywheelSwitch = 0;
  }

  if(minFireCount >= fireMode) {
  	stateSwitchSuperTrigger = 0;
  } else if (tempFlywheelSwitch && stateSwitchTrigger) {
    stateSwitchSuperTrigger = 1;
  } else if (!stateSwitchTrigger) {
    stateSwitchSuperTrigger = 0;
  }
}
